﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Web;

namespace WebSocketDemo.Project.Utility
{
    public class DataModel
    {
        public WebSocket Socket { get; set; }

        public string UserName { get; set; }
    }
}