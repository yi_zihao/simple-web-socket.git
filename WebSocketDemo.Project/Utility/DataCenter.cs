﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Web;

namespace WebSocketDemo.Project.Utility
{
    public class DataCenter
    {
        public static List<DataModel> DatamodelList = new List<DataModel>();
        private static object lock_Obj = new object();

        /// <summary>
        /// 每个用户进入到聊天室以后，这个链接就保存起来；
        /// </summary>
        /// <param name="webSocket"></param>
        /// <param name="userName"></param>
        public static void Connect(WebSocket webSocket, string userName)
        {
            lock (lock_Obj)
            {
                if (DatamodelList.Any(m => m.UserName == userName))
                {
                    //就把当前这个链接赋值给之前存在WebSocket链接
                    DatamodelList.RemoveAll(m => m.UserName == userName);
                }

                DatamodelList.Add(new DataModel()
                {
                    Socket = webSocket,
                    UserName = userName
                });
            }
        }


        public static void SendMeg(string fromUser, string Msg, CancellationToken cancellation)
        {
            lock (lock_Obj)
            {
                string sendMessage = string.Empty;

                if (Msg == "close")
                {
                    sendMessage = "退出聊天室。。。";
                }
                else
                {
                    sendMessage = Msg;
                }

                ///就把无效的WebSocket对象清理掉； 
                DatamodelList.RemoveAll(m => m.Socket.State != WebSocketState.Open);

                foreach (var dataModel in DatamodelList)
                {
                    string message = $"{DateTime.Now }  {fromUser}:{sendMessage}";
                    ArraySegment<byte> sendBuf = new ArraySegment<byte>(Encoding.UTF8.GetBytes(message));
                    if (dataModel.Socket.State == WebSocketState.Open)
                    {
                        dataModel.Socket.SendAsync(sendBuf, WebSocketMessageType.Text, true, cancellation);
                    }
                }

                if (Msg == "close")
                {
                    var fromUserModel = DatamodelList.FirstOrDefault(m => m.UserName == fromUser);
                    fromUserModel.Socket.CloseAsync(WebSocketCloseStatus.Empty, "链接关闭了", cancellation); 
                }


                //IEnumerable<DataModel> removeDataModellist = DatamodelList.Where(m => m.Socket.State != WebSocketState.Open); 
                //DatamodelList.RemoveAll(removeDataModellist.ToList());  
                //foreach (var removeModel in removeDataModellist)
                //{
                //    var model = DatamodelList.FirstOrDefault(m => m.UserName == removeModel.UserName); 
                //    DatamodelList.Remove(model);
                //}
            }
        }


    }
}