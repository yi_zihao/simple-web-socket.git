﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebSocketDemo.Project.Models
{
    public class FileUploadDTO
    {
        public int FileID { get; set; }
        public string FileName { get; set; }
        public long FileSize { get; set; }
        public long LastModified { get; set; }
        public string FileType { get; set; }
        public string UserName { get; set; }
        public int DownLoadCount { get; set; }
        public string FileSavePath { get; set; }
    }
}